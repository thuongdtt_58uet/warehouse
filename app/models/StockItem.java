package models;

import play.*;
import play.mvc.*;
import java.util.LinkedList;
import views.html.*;
import java.util.*;



/**
 * Created by Thuong on 19/03/2015.
 */
public class StockItem {
    public Warehouse warehouse;           // trường quan hệ nối với Warehouse
    public Product product;               // trường quan hệ nối với Product
    public Long quantity;

    public String toString() {
        return String.format("$d %s", quantity, product);
    }
}
